f arr = sum[x | x <- filter(odd) arr]


main = do
   inputdata <- getContents
   putStrLn $ show $ f $ map (read :: String -> Int) $ lines inputdata
