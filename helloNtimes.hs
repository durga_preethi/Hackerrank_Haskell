
hello_world n = putStrLn $ unlines (replicate n "Hello World")


main = do
    n <- readLn
    hello_world n
