import Control.Applicative
import Control.Monad
import System.IO


main :: IO ()
main = do
    n_temp <- getLine
    let n = read n_temp :: Int
    a_temp <- getLine
    let a = map read $ words a_temp :: [Int]
    b_temp <- getLine
    let b = map read $ words b_temp :: [Int]
    let h = maximum [gcd x y | x <- a , y <- b ]
    print $ [ x + y | x<-a , y<-b , gcd x y == h ] !! 0


        

getMultipleLines :: Int -> IO [String]

getMultipleLines n
    | n <= 0 = return []
    | otherwise = do          
        x <- getLine         
        xs <- getMultipleLines (n-1)    
        let ret = (x:xs)    
        return ret          

