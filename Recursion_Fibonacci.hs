fiblst  = 0:1:zipWith (+) fiblst (tail fiblst)
fib n =  fiblst!!(n-1)

-- This part is related to the Input/Output and can be used as it is
-- Do not modify it
main = do
    input <- getLine
    print . fib . (read :: String -> Int) $ input 

